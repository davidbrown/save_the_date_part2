project (
	Exercises
	LANGUAGES CXX
)

add_executable (save_the_date)

target_sources (
	save_the_date

	PRIVATE
	src/exercise_1.cpp
	src/exercise_2.cpp
	src/exercise_3.cpp
	src/exercise_4.cpp

	src/main.cpp
)

target_include_directories (
	save_the_date

	PRIVATE
	src
)

target_compile_options (
	save_the_date

	PRIVATE
	$<$<OR:$<CXX_COMPILER_ID:GNU>,$<CXX_COMPILER_ID:Clang>>:
		-Wall
	>
)

target_link_libraries (
	save_the_date

	date::date
	date::tz
)

