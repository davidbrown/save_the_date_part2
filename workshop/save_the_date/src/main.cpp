#include "exercises.h"

#include <iostream>

int main([[maybe_unused]] int argc, [[maybe_unused]] char* argv[])
{
	if (date::get_tzdb().version.empty()) {
		std::cout << "TZ database not installed; fetching...\n";
		std::cout << date::remote_version() << std::endl;
		date::remote_download(date::remote_version());
		date::remote_install(date::remote_version());
	}

	date::zoned_time zt{date::current_zone(), std::chrono::system_clock::now()};
	std::cout << "Save The Date - Part 2\n" << zt << std::endl;

	std::cout << "\n\n **** Exercise 1 **** \n";
	exercise_1();

	std::cout << "\n\n **** Exercise 2 **** \n";
	exercise_2();

	std::cout << "\n\n **** Exercise 3 **** \n";
	exercise_3();

	std::cout << "\n\n **** Exercise 4 **** \n";
	exercise_4();

	return 0;
}

