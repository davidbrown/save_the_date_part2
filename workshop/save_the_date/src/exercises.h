#ifndef SAVE_THE_DATE_EXERCISES_H
#define SAVE_THE_DATE_EXERCISES_H

#include <date/date.h>
#include <date/tz.h>

void exercise_1();
void exercise_2();
void exercise_3();
void exercise_4();
void exercise_5();

#endif
